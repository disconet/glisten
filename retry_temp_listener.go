package glisten

import (
	"net"
	"time"
)

// RetryTempListener retries temporary Accept failures with a delay and backoff
type RetryTempListener struct {
	net.Listener
}

// Accept implements the Accept method in the net.Listener interface
// it returns the next incoming connection.
// Temporary errors will not be returned, instead Accept will be
// retried using the underlying net.Listener.
func (rl *RetryTempListener) Accept() (c net.Conn, err error) {
	delay := 5 * time.Millisecond
	for {
		c, err := rl.Listener.Accept()
		ne, ok := err.(net.Error)
		if !ok || !ne.Temporary() {
			return c, err
		}
		time.Sleep(delay)
		delay = 2 * delay
		if delay > 1*time.Second {
			delay = 1 * time.Second
		}
	}
}
