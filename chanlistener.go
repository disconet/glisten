package glisten

import (
	"errors"
	"net"
	"sync"
)

type ChanListener interface {
	Close() error
	Accept() <-chan net.Conn
}

type chanListener struct {
	l      net.Listener
	close  chan struct{}
	closed chan struct{}
	conns  chan net.Conn
}

func NewChanListener(l net.Listener) ChanListener {
	cl := &chanListener{
		l:      l,
		close:  make(chan struct{}),
		closed: make(chan struct{}),
		conns:  make(chan net.Conn),
	}

	go cl.loop()
	return cl
}

func (cl *chanListener) loop() {
	var wg sync.WaitGroup

	accepted := make(chan net.Conn)

	wg.Add(1)
	go func() {
		for {
			defer wg.Done()
			con, err := cl.l.Accept()
			if err != nil {
				cl.close <- struct{}{}
				return
			}
			select {
			case cl.conns <- con:
			case <-cl.closed:
				_ = con.Close()
				return
			}
		}
	}()

	for {
		select {
		case con := <-accepted:
			cl.conns <- con
		case <-cl.close:
			_ = cl.l.Close()
			wg.Wait()
			close(cl.closed)
			close(cl.conns)
		}
	}
}

var ErrClosed = errors.New("already closed")

func (cl *chanListener) Close() error {
	select {
	case cl.close <- struct{}{}:
		<-cl.closed
		return nil
	case <-cl.closed:
		return ErrClosed
	}
}

func (cl chanListener) Accept() <-chan net.Conn {
	return cl.conns
}
