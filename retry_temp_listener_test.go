package glisten

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRetryListenerTemporary(t *testing.T) {
	assert := assert.New(t)

	ml := newMockListener("addr1")
	mc := newMockConn("la", "ra")

	ml.ProvideError(mockTemporaryError{})
	ml.ProvideConn(mc)

	rl := RetryTempListener{ml}
	c, err := rl.Accept()

	assert.NoError(err)
	assert.Equal(mc, c)
}

func TestRetryListenerNotTemporary(t *testing.T) {
	assert := assert.New(t)

	ml := newMockListener("addr1")
	ml.ProvideError(listenerClosedError{})

	rl := RetryTempListener{ml}
	c, err := rl.Accept()

	assert.Error(err)
	assert.Nil(c)
}

type mockTemporaryError struct{}

func (mte mockTemporaryError) Temporary() bool {
	return true
}

func (mte mockTemporaryError) Timeout() bool {
	return false
}

func (mte mockTemporaryError) Error() string {
	return "mockTemporaryError"
}
