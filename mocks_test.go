package glisten

import (
	"errors"
	"net"
	"sync"
	"time"
)

func newMockListener(addr string) *mockListener {
	ml := &mockListener{
		closed:    make(chan struct{}),
		addr:      mockAddr{addr},
		responses: make(chan acceptResponse, 16),
	}
	return ml
}

type mockListener struct {
	addr net.Addr

	lk sync.Mutex

	closed chan struct{}

	responses chan acceptResponse

	acceptCalled bool
	closeCalled  bool
}

func (ml *mockListener) AcceptCalled() bool {
	ml.lk.Lock()
	defer ml.lk.Unlock()
	return ml.acceptCalled
}

func (ml *mockListener) CloseCalled() bool {
	ml.lk.Lock()
	defer ml.lk.Unlock()
	return ml.closeCalled
}

func (ml *mockListener) ProvideConn(c net.Conn) {
	select {
	case ml.responses <- acceptResponse{c, nil}:
	default:
		panic("mockListener responses overflow")
	}
}

func (ml *mockListener) ProvideError(err error) {
	select {
	case ml.responses <- acceptResponse{nil, err}:
	default:
		panic("mockListener responses overflow")
	}
}

func (ml *mockListener) Accept() (net.Conn, error) {
	ml.lk.Lock()
	ml.acceptCalled = true
	ml.lk.Unlock()

	select {
	case <-ml.closed:
		return nil, listenerClosedError{}
	case resp := <-ml.responses:
		return resp.con, resp.err
	}
}

func (ml *mockListener) Addr() net.Addr {
	return ml.addr
}

func (ml *mockListener) Close() error {
	ml.lk.Lock()
	defer ml.lk.Unlock()
	if ml.closeCalled {
		return listenerClosedError{}
	}
	ml.closeCalled = true
	close(ml.closed)
	return nil
}

type mockAddr struct {
	addr string
}

func (ma mockAddr) Network() string {
	return "mock"
}
func (ma mockAddr) String() string {
	return ma.addr
}

type acceptResponse struct {
	con net.Conn
	err error
}

func newMockConn(localAddr, remoteAddr string) *mockConn {
	return &mockConn{la: mockAddr{localAddr}, ra: mockAddr{remoteAddr}}
}

type mockConn struct {
	la, ra net.Addr

	lk     sync.Mutex
	closed bool
}

func (mc *mockConn) Read(b []byte) (n int, err error) {
	panic("implement me")
}

func (mc *mockConn) Write(b []byte) (n int, err error) {
	panic("implement me")
}

func (mc *mockConn) Close() error {
	mc.lk.Lock()
	defer mc.lk.Unlock()
	if mc.closed {
		return listenerClosedError{}
	}
	mc.closed = true
	return nil
}

func (mc *mockConn) Closed() bool {
	mc.lk.Lock()
	defer mc.lk.Unlock()
	return mc.closed
}

func (mc *mockConn) LocalAddr() net.Addr {
	return mc.la
}

func (mc *mockConn) RemoteAddr() net.Addr {
	return mc.ra
}

func (mc *mockConn) SetDeadline(t time.Time) error {
	return errors.New("SetDeadline not implemented")
}

func (mc *mockConn) SetReadDeadline(t time.Time) error {
	return errors.New("SetReadDeadline not implemented")
}

func (mc *mockConn) SetWriteDeadline(t time.Time) error {
	return errors.New("SetWriteDeadline not implemented")
}
