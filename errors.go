package glisten

type listenerClosedError struct{}

func (mlc listenerClosedError) Error() string {
	return "listener closed"
}

func (mlc listenerClosedError) Temporary() bool {
	return false
}
func (mlc listenerClosedError) Timeout() bool {
	return false
}
